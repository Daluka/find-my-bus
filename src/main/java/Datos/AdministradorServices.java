/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Datos;

import Modelo.Administrador;
import java.util.List;

/**
 *
 * @author 
 */
public interface AdministradorServices {
    
    public int create(Administrador administrador);

    public List<Administrador> all();

    public Administrador selectId(Administrador administrador);

    public int update(Administrador administrador);

    public int delete(Administrador administrador);
}
