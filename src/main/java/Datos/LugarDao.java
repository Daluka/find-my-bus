/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Modelo.Lugar;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class LugarDao implements LugarServices {

    public static final String SQL_CONSULTA = "SELECT * FROM lugar";
    public static final String SQL_INSERT = "INSERT INTO lugar(id, direccion) VALUES (?,?)";
    public static final String SQL_DELETE = "DELETE FROM lugar WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE lugar SET direccion = ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM lugar WHERE id = ?";

    @Override
    public int create(Lugar lugar) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, lugar.getId());
            ps.setString(2, lugar.getDireccion());
            registros = ps.executeUpdate();
            res = ps.getGeneratedKeys();
            res.next();
            lugar.setId(res.getInt(1));
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Lugar> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Lugar> lugars = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String direccion = res.getString("direccion");
                Lugar lugar = new Lugar(id, direccion);
                lugars.add(lugar);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return lugars;
    }

    @Override
    public Lugar selectId(Lugar lugar) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Lugar registroLugar = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, lugar.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String direccion = res.getString("direccion");
            registroLugar = new Lugar(id, direccion);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroLugar;
    }

    @Override
    public int update(Lugar lugar) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(1, lugar.getId());
            ps.setString(2, lugar.getDireccion());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Lugar lugar) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, lugar.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
