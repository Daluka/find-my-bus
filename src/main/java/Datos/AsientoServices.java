/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Datos;

import Modelo.Asiento;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface AsientoServices {
    
    public int create(Asiento asiento);

    public List<Asiento> all();

    public Asiento selectId(Asiento asiento);

    public int update(Asiento asiento);

    public int delete(Asiento asiento);
}
