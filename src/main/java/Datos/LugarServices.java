/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Datos;

import Modelo.Lugar;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface LugarServices {

    public int create(Lugar lugar);

    public List<Lugar> all();

    public Lugar selectId(Lugar lugar);

    public int update(Lugar lugar);

    public int delete(Lugar lgar);
}
