/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Modelo.PuntoDeControl;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class PuntoDeControlDao implements PuntoDeControlServices {

    public static final String SQL_INSERT = "INSERT INTO punto_de_control(id_buseta, id_lugar, hora) VALUES (?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM punto_de_control";
    public static final String SQL_CONSULTAID = "SELECT * FROM punto_de_control WHERE id_buseta = ? AND id_lugar = ?";
    public static final String SQL_DELETE = "DELETE FROM punto_de_control WHERE id_buseta = ? AND id_lugar = ?";
    public static final String SQL_UPDATE = "UPDATE punto_de_control SET hora = ? WHERE id_buseta = ? AND id_lugar = ?";

    @Override
    public int create(PuntoDeControl puntoDeControl) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setInt(1, puntoDeControl.getIdBuseta());
            ps.setInt(2, puntoDeControl.getIdLugar());
            ps.setTime(3, puntoDeControl.getHora());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<PuntoDeControl> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<PuntoDeControl> puntosDeControl = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int idBuseta = res.getInt("id_buseta");
                int idLugar = res.getInt("id_lugar");
                Time hora = res.getTime("hora");
                PuntoDeControl puntoDeControl = new PuntoDeControl(idBuseta, idLugar, hora);
                puntosDeControl.add(puntoDeControl);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return puntosDeControl;
    }

    @Override
    public PuntoDeControl selectId(PuntoDeControl puntoDeControl) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        PuntoDeControl registroPunto = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, puntoDeControl.getIdBuseta());
            ps.setInt(2, puntoDeControl.getIdLugar());
            res = ps.executeQuery();
            res.absolute(1);
            int idBuseta = res.getInt("id_buseta");
            int idLugar = res.getInt("id_lugar");
            Time hora = res.getTime("hora");
            registroPunto = new PuntoDeControl(idBuseta, idLugar, hora);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroPunto;
    }

    @Override
    public int update(PuntoDeControl puntoDeControl) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setTime(1, puntoDeControl.getHora());
            ps.setInt(2, puntoDeControl.getIdBuseta());
            ps.setInt(3, puntoDeControl.getIdLugar());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(PuntoDeControl puntoDeControl) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, puntoDeControl.getIdBuseta());
            ps.setInt(2, puntoDeControl.getIdLugar());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
