/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Modelo.Pasaje;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class PasajeDao implements PasajeServices {

    public static final String SQL_CONSULTA = "SELECT * FROM pasaje";
    public static final String SQL_INSERT = "INSERT INTO pasaje(precio, id_pasajero, id_asiento, fecha) VALUES (?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM pasaje WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE pasaje SET precio = ?, id_pasajero = ?, id_asiento = ?, fecha= ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM pasaje WHERE id = ?";

    @Override
    public int create(Pasaje pasaje) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setDouble(1, pasaje.getPrecio());
            ps.setString(2, pasaje.getIdPasajero());
            ps.setInt(3, pasaje.getIdAsiento());
            ps.setDate(4, pasaje.getFecha());
            registros = ps.executeUpdate();
            res = ps.getGeneratedKeys();
            res.next();
            pasaje.setId(res.getInt(1));
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Pasaje> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Pasaje> pasajes = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                double precio = res.getDouble("precio");
                String id_pasajero = res.getString("id_pasajero");
                int id_asiento = res.getInt("id_asiento");
                Date fecha = res.getDate("fecha");
                Pasaje pasaje = new Pasaje(id, precio, id_pasajero, id_asiento, fecha);
                pasajes.add(pasaje);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return pasajes;
    }

    @Override
    public Pasaje selectId(Pasaje pasaje) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Pasaje registroPasaje = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, pasaje.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            double precio = res.getDouble("precio");
            String id_pasajero = res.getString("id_pasajero");
            int id_asiento = res.getInt("id_asiento");
            Date fecha = res.getDate("fecha");
            registroPasaje = new Pasaje(id, precio, id_pasajero, id_asiento, fecha);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroPasaje;
    }

    @Override
    public int update(Pasaje pasaje) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setDouble(1, pasaje.getPrecio());
            ps.setString(2, pasaje.getIdPasajero());
            ps.setInt(3, pasaje.getIdAsiento());
            ps.setDate(4, pasaje.getFecha());
            ps.setInt(5, pasaje.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Pasaje pasaje) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, pasaje.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }
}
