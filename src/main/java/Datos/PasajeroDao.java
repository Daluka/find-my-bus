/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Modelo.Pasajero;
import Red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class PasajeroDao implements PasajeroServices {

    public static final String SQL_CONSULTA = "SELECT * FROM pasajero C, persona P WHERE C.id = P.id";
    public static final String SQL_INSERT = "INSERT INTO pasajero(id) VALUES (?)";
    public static final String SQL_PINSERT = "INSERT INTO persona(id, nombre, apellido, direccion, correo, contrasenia) VALUES (?,?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM pasajero WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE pasajero SET nombre = ?, apellido = ?, direccion = ?, correo = ?, contrasenia = ? WHERE id = ?";
    public static final String SQL_PUPDATE = "UPDATE persona SET nombre = ?, apellido = ?, direccion = ?, correo = ?, contrasenia = ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM pasajero C, persona P WHERE C.id = ? AND C.id = P.id";

    @Override
    public int create(Pasajero pasajero) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_PINSERT);
            ps.setString(1, pasajero.getId());
            ps.setString(2, pasajero.getNombre());
            ps.setString(3, pasajero.getApellido());
            ps.setString(4, pasajero.getDireccion());
            ps.setString(5, pasajero.getCorreo());
            ps.setString(6, pasajero.getContrasenia());
            registros = ps.executeUpdate();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, pasajero.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Pasajero> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Pasajero> pasajeroes = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String id = res.getString("id");
                String nombre = res.getString("nombre");
                String apellido = res.getString("apellido");
                String correo = res.getString("correo");
                String direccion = res.getString("direccion");
                String contrasenia = res.getString("contrasenia");

                Pasajero pasajero = new Pasajero(id, nombre, apellido, direccion, correo, contrasenia);
                pasajeroes.add(pasajero);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return pasajeroes;
    }

    @Override
    public Pasajero selectId(Pasajero pasajero) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Pasajero registroPasajero = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setString(1, pasajero.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String id = res.getString("id");
            String nombre = res.getString("nombre");
            String apellido = res.getString("apellido");
            String correo = res.getString("correo");
            String direccion = res.getString("direccion");
            String contrasenia = res.getString("contrasenia");

            registroPasajero = new Pasajero(id, nombre, apellido, direccion, correo, contrasenia);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroPasajero;
    }

    @Override
    public int update(Pasajero pasajero) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_PUPDATE);
            ps.setString(1, pasajero.getNombre());
            ps.setString(2, pasajero.getApellido());
            ps.setString(3, pasajero.getDireccion());
            ps.setString(4, pasajero.getCorreo());
            ps.setString(5, pasajero.getContrasenia());
            ps.setString(6, pasajero.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Pasajero pasajero) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, pasajero.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
