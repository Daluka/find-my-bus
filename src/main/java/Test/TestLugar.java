/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.LugarDao;
import Modelo.Lugar;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestLugar {

    public static void main(String[] args) {
        int n = (int) (Math.random() * (10000)) + 3000;
        LugarDao lugarDB = new LugarDao();
        Lugar lugarNuevo = new Lugar(0, "Direccion" + n);
        lugarDB.create(lugarNuevo);

        System.out.println("Se creo el lugar con id =" + lugarDB.selectId(lugarNuevo).getId());

        lugarNuevo.setDireccion("Direccion" + (n - 1));
        lugarDB.update(lugarNuevo);

        n++;
        Lugar lugarNuevo2 = new Lugar(0, "Direccion" + n);
        lugarDB.create(lugarNuevo2);

        lugarDB.delete(lugarNuevo2);

        List<Lugar> lugares = lugarDB.all();
        System.out.println("\nLista de lugares");
        for (Lugar x : lugares) {
            System.out.println("Direccion = " + x.getDireccion());
        }
    }

}
