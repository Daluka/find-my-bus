/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.CompaniaDao;
import Modelo.Compania;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestCompania {

    public static void main(String[] args) {
        CompaniaDao companiaDB = new CompaniaDao();
        int n = (int) (Math.random() * (10000)) + 3000;
        Compania companiaNueva = new Compania(Integer.toString(n), "Compania " + n, "Direccion " + n);

        companiaDB.create(companiaNueva);

        System.out.println("Se creo la compania con id =" + companiaDB.selectId(companiaNueva).getId());

        companiaNueva.setDireccion("DireccionNueva " + n);
        companiaDB.update(companiaNueva);

        n++;
        Compania companiaNueva2 = new Compania(Integer.toString(n), "Compania " + n, "Direccion " + n);
        companiaDB.create(companiaNueva2);

        companiaDB.delete(companiaNueva2);

        List<Compania> companias = companiaDB.all();
        System.out.println("\nLista de companias");
        for (Compania x : companias) {
            System.out.println("Nombre = " + x.getNombre());
        }
    }

}
