/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.AdministradorDao;
import Modelo.Administrador;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestAdministrador {
    
    public static void main(String[] args) {
                int n = (int) (Math.random() * (10000)) + 3000;
        AdministradorDao adminD = new AdministradorDao();
        Administrador admin = new Administrador(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n, "1");
        adminD.create(admin);
        System.out.println("Se creo el administrador con id =" + adminD.selectId(admin).getId());

        admin.setDireccion("DireccionNueva " + n);
        adminD.update(admin);

        n++;
        Administrador admin2 = new Administrador(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n, "1");
        adminD.create(admin2);

        adminD.delete(admin2);

        List<Administrador> administradores = adminD.all();
        System.out.println("\nLista de administradores");
        for (Administrador x : administradores) {
            System.out.println("Nombre = " + x.getNombre() + ",   idCompania = " + x.getIdCompania());
        }
    }
}
