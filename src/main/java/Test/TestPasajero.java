/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.PasajeroDao;
import Modelo.Pasajero;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestPasajero {

    public static void main(String[] args) {
        int n = (int) (Math.random() * (10000)) + 3000;
        PasajeroDao pasajeroDB = new PasajeroDao();
        Pasajero pasajeroNuevo = new Pasajero(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        pasajeroDB.create(pasajeroNuevo);
        System.out.println("Se creo el pasajero con id =" + pasajeroDB.selectId(pasajeroNuevo).getId());

        pasajeroNuevo.setDireccion("DireccionNueva " + n);
        pasajeroDB.update(pasajeroNuevo);

        n++;
        Pasajero pasajeroNuevo2 = new Pasajero(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        pasajeroDB.create(pasajeroNuevo2);

        pasajeroDB.delete(pasajeroNuevo2);

        List<Pasajero> pasajeros = pasajeroDB.all();
        System.out.println("\nLista de pasajeros");
        for (Pasajero x : pasajeros) {
            System.out.println("Nombre = " + x.getNombre());
        }
    }

}
