/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.PuntoDeControlDao;
import Modelo.PuntoDeControl;
import java.sql.Time;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestPuntoDeControl {

    public static void main(String[] args) {
        PuntoDeControlDao puntoDB = new PuntoDeControlDao();
        PuntoDeControl puntoNuevo = new PuntoDeControl(3, 7, new Time(System.currentTimeMillis()));
        puntoDB.create(puntoNuevo);
        System.out.println("Se creo un punto para el lugar con id " + puntoDB.selectId(puntoNuevo).getIdLugar());

        puntoNuevo.setHora(new Time(System.currentTimeMillis()));
        puntoDB.update(puntoNuevo);
        puntoDB.delete(puntoNuevo);

        List<PuntoDeControl> puntos = puntoDB.all();
        System.out.println("\nLista de puntos de control");
        for (PuntoDeControl x : puntos) {
            System.out.println("Lugar: " + x.getIdLugar() + ", Buseta : " + x.getIdBuseta() + ", Hora : " + x.getHora());
        }
    }
}
