/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.UbicacionActualDao;
import Modelo.UbicacionActual;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestUbicacionActual {

    public static void main(String[] args) {
        int n = (int) (Math.random() * (10000)) + 3000;
        UbicacionActualDao ubicacionDB = new UbicacionActualDao();
        UbicacionActual ubicacionNueva = new UbicacionActual(0, n, n, n);
        ubicacionDB.create(ubicacionNueva);

        System.out.println("Se creo la ubicacion con id =" + ubicacionDB.selectId(ubicacionNueva).getId());

        ubicacionNueva.setxValue(n + 1);
        ubicacionDB.update(ubicacionNueva);

        n++;
        UbicacionActual ubicacionNueva2 = new UbicacionActual(0, n, n, n);
        ubicacionDB.create(ubicacionNueva2);

        ubicacionDB.delete(ubicacionNueva2);

        List<UbicacionActual> ubicaciones = ubicacionDB.all();
        System.out.println("Hay " + ubicaciones.size() + " ubicaciones");
    }
}
