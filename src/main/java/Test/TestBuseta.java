/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.BusetaDao;
import Modelo.Buseta;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestBuseta {

    public static void main(String[] args) {
        BusetaDao busetaDao = new BusetaDao();
        int n = (int) (Math.random() * (10000)) + 3000;
        Buseta busetaNueva = new Buseta(0, "placa" + n, "10898", "10374", 2);

        busetaDao.create(busetaNueva);

        System.out.println("Se creo la buseta con placa =" + busetaDao.selectId(busetaNueva).getPlaca());

        busetaNueva.setPlaca("Placa" + n);
        busetaDao.update(busetaNueva);

        n++;
        Buseta busetaNueva2 = new Buseta(0, "placa" + n, "10898", "10374", 2);
        busetaDao.create(busetaNueva2);

        busetaDao.delete(busetaNueva2);

        List<Buseta> busetas = busetaDao.all();
        System.out.println("\nLista de busetas");
        for (Buseta x : busetas) {
            System.out.println("Placa = " + x.getPlaca());
        }
    }

}
