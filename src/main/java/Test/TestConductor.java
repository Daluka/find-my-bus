/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.ConductorDao;
import Modelo.Conductor;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestConductor {

    public static void main(String[] args) {
        int n = (int) (Math.random() * (10000)) + 3000;
        ConductorDao conductorDB = new ConductorDao();
        Conductor conductorNuevo = new Conductor(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        conductorDB.create(conductorNuevo);
        System.out.println("Se creo el conductor con id =" + conductorDB.selectId(conductorNuevo).getId());

        conductorNuevo.setDireccion("DireccionNueva " + n);
        conductorDB.update(conductorNuevo);

        n++;
        Conductor conductorNuevo2 = new Conductor(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        conductorDB.create(conductorNuevo2);

        conductorDB.delete(conductorNuevo2);

        List<Conductor> conductores = conductorDB.all();
        System.out.println("\nLista de conductores");
        for (Conductor x : conductores) {
            System.out.println("Nombre = " + x.getNombre());
        }
    }
}
