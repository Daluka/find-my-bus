/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.AdministradorDao;
import Datos.AsientoDao;
import Datos.BusetaDao;
import Datos.CompaniaDao;
import Datos.ConductorDao;
import Datos.LugarDao;
import Datos.PasajeDao;
import Datos.PasajeroDao;
import Datos.PuntoDeControlDao;
import Datos.UbicacionActualDao;
import Modelo.Administrador;
import Modelo.Asiento;
import Modelo.Buseta;
import Modelo.Compania;
import Modelo.Conductor;
import Modelo.Lugar;
import Modelo.Pasaje;
import Modelo.Pasajero;
import Modelo.PuntoDeControl;
import Modelo.UbicacionActual;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

/**
 *
 * @author Daniela
 */
public class Test {

    public static void main(String[] args) {
        // Se recomienda utilizar las clases de Prueba especificas para cada Entity
        
        //probarCompaniaDao();
        //probarAdministradorDao();
        //probarConductorDao();
        //probarPasajeroDao();
        //probarUbicacionActualDao();
        //probarLugarDao();
        //probarBusetaDao();
        //probarAsientoDao();
        //probarPasajeDao();
        //probarPuntoDeControlDao();
    }

    public static void probarAdministradorDao() {
        int n = (int) (Math.random() * (10000)) + 3000;
        AdministradorDao adminD = new AdministradorDao();
        Administrador admin = new Administrador(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n, "1");
        adminD.create(admin);
        System.out.println("Se creo el administrador con id =" + adminD.selectId(admin).getId());

        admin.setDireccion("DireccionNueva " + n);
        adminD.update(admin);

        n++;
        Administrador admin2 = new Administrador(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n, "1");
        adminD.create(admin2);

        adminD.delete(admin2);

        List<Administrador> administradores = adminD.all();
        System.out.println("\nLista de administradores");
        for (Administrador x : administradores) {
            System.out.println("Nombre = " + x.getNombre() + ",   idCompania = " + x.getIdCompania());
        }
    }

    public static void probarAsientoDao() {
        AsientoDao asientoDao = new AsientoDao();
        int n = (int) (Math.random() * (100)) + 1;
        Asiento asientoNuevo = new Asiento(0, 1, n, n);

        asientoDao.create(asientoNuevo);

        System.out.println("Se creo el asiento con id =" + asientoDao.selectId(asientoNuevo).getId());

        asientoNuevo.setFila(n + 1);
        asientoDao.update(asientoNuevo);

        n++;
        Asiento asientoNuevo2 = new Asiento(0, 1, n, n);
        asientoDao.create(asientoNuevo2);

        asientoDao.delete(asientoNuevo2);

        List<Asiento> asientos = asientoDao.all();
        System.out.println("Hay un total de " + asientos.size() + " asientos");
    }

    public static void probarBusetaDao() {
        BusetaDao busetaDao = new BusetaDao();
        int n = (int) (Math.random() * (10000)) + 3000;
        Buseta busetaNueva = new Buseta(0, "placa" + n, "10898", "10374", 2);

        busetaDao.create(busetaNueva);

        System.out.println("Se creo la buseta con placa =" + busetaDao.selectId(busetaNueva).getPlaca());

        busetaNueva.setPlaca("Placa" + n);
        busetaDao.update(busetaNueva);

        n++;
        Buseta busetaNueva2 = new Buseta(0, "placa" + n, "10898", "10374", 2);
        busetaDao.create(busetaNueva2);

        busetaDao.delete(busetaNueva2);

        List<Buseta> busetas = busetaDao.all();
        System.out.println("\nLista de busetas");
        for (Buseta x : busetas) {
            System.out.println("Placa = " + x.getPlaca());
        }
    }

    public static void probarCompaniaDao() {
        CompaniaDao companiaDB = new CompaniaDao();
        int n = (int) (Math.random() * (10000)) + 3000;
        Compania companiaNueva = new Compania(Integer.toString(n), "Compania " + n, "Direccion " + n);

        companiaDB.create(companiaNueva);

        System.out.println("Se creo la compania con id =" + companiaDB.selectId(companiaNueva).getId());

        companiaNueva.setDireccion("DireccionNueva " + n);
        companiaDB.update(companiaNueva);

        n++;
        Compania companiaNueva2 = new Compania(Integer.toString(n), "Compania " + n, "Direccion " + n);
        companiaDB.create(companiaNueva2);

        companiaDB.delete(companiaNueva2);

        List<Compania> companias = companiaDB.all();
        System.out.println("\nLista de companias");
        for (Compania x : companias) {
            System.out.println("Nombre = " + x.getNombre());
        }
    }

    public static void probarConductorDao() {
        int n = (int) (Math.random() * (10000)) + 3000;
        ConductorDao conductorDB = new ConductorDao();
        Conductor conductorNuevo = new Conductor(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        conductorDB.create(conductorNuevo);
        System.out.println("Se creo el conductor con id =" + conductorDB.selectId(conductorNuevo).getId());

        conductorNuevo.setDireccion("DireccionNueva " + n);
        conductorDB.update(conductorNuevo);

        n++;
        Conductor conductorNuevo2 = new Conductor(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        conductorDB.create(conductorNuevo2);

        conductorDB.delete(conductorNuevo2);

        List<Conductor> conductores = conductorDB.all();
        System.out.println("\nLista de conductores");
        for (Conductor x : conductores) {
            System.out.println("Nombre = " + x.getNombre());
        }
    }

    public static void probarLugarDao() {
        int n = (int) (Math.random() * (10000)) + 3000;
        LugarDao lugarDB = new LugarDao();
        Lugar lugarNuevo = new Lugar(0, "Direccion" + n);
        lugarDB.create(lugarNuevo);

        System.out.println("Se creo el lugar con id =" + lugarDB.selectId(lugarNuevo).getId());

        lugarNuevo.setDireccion("Direccion" + (n - 1));
        lugarDB.update(lugarNuevo);

        n++;
        Lugar lugarNuevo2 = new Lugar(0, "Direccion" + n);
        lugarDB.create(lugarNuevo2);

        lugarDB.delete(lugarNuevo2);

        List<Lugar> lugares = lugarDB.all();
        System.out.println("\nLista de lugares");
        for (Lugar x : lugares) {
            System.out.println("Direccion = " + x.getDireccion());
        }
    }

    public static void probarPasajeDao() {
        PasajeDao pasajeDao = new PasajeDao();
        int n = (int) (Math.random() * (10000)) + 1000;
        Pasaje pasajeNuevo = new Pasaje(0, n, "3853", 1, new Date(System.currentTimeMillis()));

        pasajeDao.create(pasajeNuevo);

        System.out.println("Se creo el pasaje con id =" + pasajeDao.selectId(pasajeNuevo).getId());

        pasajeNuevo.setPrecio(n + 10);
        pasajeDao.update(pasajeNuevo);

        n++;
        Pasaje pasajeNuevo2 = new Pasaje(0, n, "3853", 1, new Date(System.currentTimeMillis()));
        pasajeDao.create(pasajeNuevo2);

        pasajeDao.delete(pasajeNuevo2);

        List<Pasaje> pasajes = pasajeDao.all();
        System.out.println("Hay un total de " + pasajes.size() + " pasajes");
    }

    public static void probarPasajeroDao() {
        int n = (int) (Math.random() * (10000)) + 3000;
        PasajeroDao pasajeroDB = new PasajeroDao();
        Pasajero pasajeroNuevo = new Pasajero(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        pasajeroDB.create(pasajeroNuevo);
        System.out.println("Se creo el pasajero con id =" + pasajeroDB.selectId(pasajeroNuevo).getId());

        pasajeroNuevo.setDireccion("DireccionNueva " + n);
        pasajeroDB.update(pasajeroNuevo);

        n++;
        Pasajero pasajeroNuevo2 = new Pasajero(Integer.toString(n), "nombres" + n, "apellidos" + n, "direccion" + n, "correo" + n, "password" + n);
        pasajeroDB.create(pasajeroNuevo2);

        pasajeroDB.delete(pasajeroNuevo2);

        List<Pasajero> pasajeros = pasajeroDB.all();
        System.out.println("\nLista de pasajeros");
        for (Pasajero x : pasajeros) {
            System.out.println("Nombre = " + x.getNombre());
        }
    }

    public static void probarPuntoDeControlDao() {
        PuntoDeControlDao puntoDB = new PuntoDeControlDao();
        PuntoDeControl puntoNuevo = new PuntoDeControl(1, 3, new Time(System.currentTimeMillis()));
        puntoDB.create(puntoNuevo);
        System.out.println("Se creo un punto para el lugar con id " + puntoDB.selectId(puntoNuevo).getIdLugar());

        puntoNuevo.setHora(new Time(System.currentTimeMillis()));
        puntoDB.update(puntoNuevo);
        PuntoDeControl puntoNuevo2 = new PuntoDeControl(1, 5, new Time(System.currentTimeMillis()));
        puntoDB.create(puntoNuevo2);

        puntoDB.delete(puntoNuevo2);

        List<PuntoDeControl> puntos = puntoDB.all();
        System.out.println("\nLista de puntos de control");
        for (PuntoDeControl x : puntos) {
            System.out.println("Lugar: " + x.getIdLugar() + ", Buseta : " + x.getIdBuseta() + ", Hora : " + x.getHora());
        }
    }

    public static void probarUbicacionActualDao() {
        int n = (int) (Math.random() * (10000)) + 3000;
        UbicacionActualDao ubicacionDB = new UbicacionActualDao();
        UbicacionActual ubicacionNueva = new UbicacionActual(0, n, n, n);
        ubicacionDB.create(ubicacionNueva);

        System.out.println("Se creo la ubicacion con id =" + ubicacionDB.selectId(ubicacionNueva).getId());

        ubicacionNueva.setxValue(n + 1);
        ubicacionDB.update(ubicacionNueva);

        n++;
        UbicacionActual ubicacionNueva2 = new UbicacionActual(0, n, n, n);
        ubicacionDB.create(ubicacionNueva2);

        ubicacionDB.delete(ubicacionNueva2);

        List<UbicacionActual> ubicaciones = ubicacionDB.all();
        System.out.println("Hay " + ubicaciones.size() + " ubicaciones");

    }
}
