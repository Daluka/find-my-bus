/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.PasajeDao;
import Modelo.Pasaje;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestPasaje {

    public static void main(String[] args) {
        PasajeDao pasajeDao = new PasajeDao();
        int n = (int) (Math.random() * (10000)) + 1000;
        Pasaje pasajeNuevo = new Pasaje(0, n, "3853", 1, new Date(System.currentTimeMillis()));

        pasajeDao.create(pasajeNuevo);

        System.out.println("Se creo el pasaje con id =" + pasajeDao.selectId(pasajeNuevo).getId());

        pasajeNuevo.setPrecio(n + 10);
        pasajeDao.update(pasajeNuevo);

        n++;
        Pasaje pasajeNuevo2 = new Pasaje(0, n, "3853", 1, new Date(System.currentTimeMillis()));
        pasajeDao.create(pasajeNuevo2);

        pasajeDao.delete(pasajeNuevo2);

        List<Pasaje> pasajes = pasajeDao.all();
        System.out.println("Hay un total de " + pasajes.size() + " pasajes");
    }

}
