/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Test;

import Datos.AsientoDao;
import Modelo.Asiento;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class TestAsiento {
    public static void main(String[] args) {
                AsientoDao asientoDao = new AsientoDao();
        int n = (int) (Math.random() * (100)) + 1;
        Asiento asientoNuevo = new Asiento(0, 1, n, n);

        asientoDao.create(asientoNuevo);

        System.out.println("Se creo el asiento con id =" + asientoDao.selectId(asientoNuevo).getId());

        asientoNuevo.setFila(n + 1);
        asientoDao.update(asientoNuevo);

        n++;
        Asiento asientoNuevo2 = new Asiento(0, 1, n, n);
        asientoDao.create(asientoNuevo2);

        asientoDao.delete(asientoNuevo2);

        List<Asiento> asientos = asientoDao.all();
        System.out.println("Hay un total de " + asientos.size() + " asientos");
    }
}
