/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Daniela
 */
public class Asiento {

    private int id;
    private int idBuseta;
    private int fila;
    private int columna;

    public Asiento() {
    }

    public Asiento(int id, int idBuseta, int fila, int columna) {
        this.id = id;
        this.idBuseta = idBuseta;
        this.fila = fila;
        this.columna = columna;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdBuseta() {
        return idBuseta;
    }

    public void setIdBuseta(int idBuseta) {
        this.idBuseta = idBuseta;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }



}
