/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Daniela
 */
public class Conductor extends Persona {

    public Conductor() {
    }

    public Conductor(String id, String nombre, String apellido, String direccion, String correo, String contrasenia) {
        super(id, nombre, apellido, direccion, correo, contrasenia);
    }

}
