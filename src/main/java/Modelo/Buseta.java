/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Daniela
 */
public class Buseta {
    private int id;
    private String placa;
    private String idCompania;
    private String idConductor;
    private int idUbicacionActual;

    public Buseta() {
    }

    public Buseta(int id, String placa, String idCompania, String idConductor, int idUbicacionActual) {
        this.id = id;
        this.placa = placa;
        this.idCompania = idCompania;
        this.idConductor = idConductor;
        this.idUbicacionActual = idUbicacionActual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getIdCompania() {
        return idCompania;
    }

    public void setIdCompania(String idCompania) {
        this.idCompania = idCompania;
    }

    public String getIdConductor() {
        return idConductor;
    }

    public void setIdConductor(String idConductor) {
        this.idConductor = idConductor;
    }

    public int getIdUbicacionActual() {
        return idUbicacionActual;
    }

    public void setIdUbicacionActual(int idUbicacionActual) {
        this.idUbicacionActual = idUbicacionActual;
    }
   
}
